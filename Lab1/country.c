﻿/*
 *  Задание #6
 *  Автор: Запорожец Александр ИУ3-83
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char* argv[])
{
    COUNTRY* list;

    if (argc == 1)
        return 0;
    /* Загрузка списка */
    list = load();

    if (strcmp(argv[1], "add") == 0) {

        if (argc == 2) { /* если написали add, но без страны */
            printf("TWO_ARGS\n\n");
            clear(&list);
            return 0;
        }

        int population = 0;
        int area = 0;

        if (argc == 4) {
            population = atoi(argv[3]);
        }
        else if (argc == 5) {
            population = atoi(argv[3]);
            area = atoi(argv[4]);
        }

        add(&list, argv[2], population, area);
    }
    else if (strcmp(argv[1], "dump") == 0) {
        if (argc == 3) {
            if (strcmp(argv[2], "-n") == 0) {
                sort_by_name(&list);
            }
            else if (strcmp(argv[2], "-p") == 0) {
                sort_by_population(&list);
            }
            else if (strcmp(argv[2], "-a") == 0) {
                sort_by_area(&list);
            }

        }
        printf("\t%s\t|\t%s\t|\t%s\n", "NAME", "POPULATION", "AREA");
        printf("\n");
        dump(list);
    }
    else if (strcmp(argv[1], "count") == 0) {
        printf("%d\n", count(list));
    }
    else if (strcmp(argv[1], "delete") == 0) {
        COUNTRY* country_to_delete = find(list, argv[2]);
        if (!country_to_delete) {
            printf("\n-----УДАЛЯЕМАЯ СТРАНА НЕ НАЙДЕНА-----\n\n");
            return 0;
        }
        print_country(country_to_delete);
        delete(&list, country_to_delete); /* находим страну и удаляем */
    }
    else if (strcmp(argv[1], "view") == 0) {
        COUNTRY* country_to_view = find(list, argv[2]);
        if (country_to_view)
            print_country(country_to_view);
        else
            printf("\n-----СТРАНА НЕ НАЙДЕНА-----\n\n");
    }

    save(list);
    /* Удаление списка из динамической памяти */
    clear(&list);

    return 0;
}
