#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"


#define DBNAME "./countries_table.csv"
#define MAX_LINE_LENGTH 300


/* ��� ��������� �� ������� ��� ��������� ��������� ������ */
typedef int (*CMP_FUNC)(COUNTRY* v1, COUNTRY* v2);

/*
 * �������� ������������ ������ ��� ������ �������� list, �������������� ����, ��������� ������� � ������ ������
 * �������� ��������� �� ������ ������. ���������� 1 ���� �� ������� �������� ������ ��� ��������  ��� 0 ���� ����� ��������������� 
 */
int add(COUNTRY** list, char* name, int population, int area)
{
    /*
        @list - ������ (���� ��)
        @name - �������� ������
        @population - �����, ���������� ���������
        @area - ������� ��.��

        ������� ���������� ������ �������� � ������
    */
    if (find(*list, name)) {
        printf("\n\n ������ � ����� �������� ��� ���������� \n\n");
        return 0;
    }

    COUNTRY* new_country = (COUNTRY*)malloc(sizeof(COUNTRY));

    if (new_country == NULL)
        return 1;

    strcpy(new_country->name, name);
    new_country->population = population;
    new_country->area = area;

    new_country->next = *list;
    *list = new_country;

    return 0; /* ok */
}

/* ������� ������� ������ list � ����������� ���������� �� ���� ������.
 *   ���� ������� ������, �������� ��������� �� ������
 */
void delete(COUNTRY** list, COUNTRY* v)
{
    /*
        @list - ������, � ������� �������� ��
        @v - ��������� ������, ������� ����� ������� �� ������ list
    */
    if (v != NULL) { /* ���� �� ������ �� ����� � ������� find() */
        COUNTRY* p = *list;
        COUNTRY* prev = NULL;

        while (p != NULL) {
            /* ����� �� ������, ������� ���� ������� */
            if (strcmp(p->name, v->name) == 0) {
                if (prev) {
                    prev->next = p->next;
                }
                else {
                    /* ���� prev == NULL, �� ������� ������ ������� ������ */
                    *list = p->next;
                }
                free(p); /* ����������� ������, ��������� ��� ������� */
                break;
            }
            prev = p;
            p = p->next;
        }
    }
}

/*
 * ���� � ������ ������ � �������� ������ name. ���������� ��������� ��
 * ��������� ������� ������ ��� NULL, ���� ������ � ����� ��������� ����������
 * � ������.
 */
COUNTRY* find(COUNTRY* list, char* name)
{
    /*
        @list - ������, � ������� �������� ��
        @name - �������� ������, ������� �� ����
    */

    COUNTRY* p = list;
    while (p != NULL) {
        if (strcmp(p->name, name) == 0)
            return p;

        p = p->next;
    }
    return NULL;
}

 // ���������� ���������� ��������� � ������
int count(COUNTRY* list)
{
    int cnt = 0;

    printf("::COUNT\n");
    while (list != NULL) {
        cnt++;
        list = list->next;
    }

    return cnt;
}

//������� ���������� ������� ���������� �� �������� ������
void quick_sort(COUNTRY** ca, int first, int last, CMP_FUNC compare)
{
    int i, j;
    COUNTRY* v;
    COUNTRY* p;

    i = first;
    j = last;
    v = ca[(first + last) / 2];

    do {
        while (compare(ca[i], v) < 0) i++;
        while (compare(ca[j], v) > 0) j--;

        if (i <= j) {
            if (compare(ca[i], ca[j]) > 0) {
                p = ca[j];
                ca[j] = ca[i];
                ca[i] = p;
            }
            i++; j--;
        }
    } while (i <= j);

    if (i < last)
        quick_sort(ca, i, last, compare);

    if (first < j)
        quick_sort(ca, first, j, compare);
}

int compare_name(COUNTRY* v1, COUNTRY* v2)
{
    return strcmp(v1->name, v2->name);
}

int compare_area(COUNTRY* v1, COUNTRY* v2)
{
    return v1->area - v2->area;
}

int compare_population(COUNTRY* v1, COUNTRY* v2)
{
    return v1->population - v2->population;
}

/*
 * ��������� ������ �� ������ ����� � ���������� ������� ������� �����������
 * ����������. ��������� �� ������ ������ ����� ���������� � ����������
 * ����������
 */
int sort(COUNTRY** list, CMP_FUNC compare)
{
    int cnt, i;
    COUNTRY* p, ** ca = NULL;

    cnt = count(*list);
    if (cnt < 2)
        return 0;

    // �������� ������ ��� ������ ����������
    ca = (COUNTRY**)malloc(cnt * sizeof(COUNTRY*));
    if (!ca)
        return 1;

    // ��������� ������ ����������� �� �������� ������
    ca[0] = *list;
    for (i = 1; i < cnt; i++)
        ca[i] = ca[i - 1]->next;

    quick_sort(ca, 0, cnt - 1, compare);

    // ��������� ������ ���������� �� ���������������� �������
    *list = NULL;
    while (cnt > 0) {
        ca[cnt - 1]->next = *list;
        *list = ca[cnt - 1];
        cnt--;
    }
    free(ca);
    return 0;
}


int sort_by_name(COUNTRY** list)
{
    printf("::SORT_BY_NAME:\n");
    sort(list, compare_name);
}

int sort_by_area(COUNTRY** list)
{
    printf("::SORT_BY_AREA:\n");
    sort(list, compare_area);
}

int sort_by_population(COUNTRY** list)
{
    printf("::SORT_BY_POPULATION:\n");
    sort(list, compare_population);
}

void print_country(COUNTRY* p)
{
    printf("%-15.20s\t|\t", p->name);
    if (p->population == 0) {
        printf("%-15.22s\t|\t", "�� ��������");
    }
    else
        printf("%10d\t|\t", p->population);
    if (p->area == 0) {
        printf("%-15.22s\n", "�� ��������");
    }
    else
        printf("%10d\n", p->area);
    printf("-------------------------------------------------------------\n");
}

void dump(COUNTRY* list)
{
    COUNTRY* p;
    p = list; /* ������ ������ */
    while (p != NULL) {
        print_country(p);
        p = p->next;
    }
}

/*
 * �������� ���� ��������� ������ �
 * ������������ ���������� ��� ��� �������� ������
 */
void clear(COUNTRY** list)
{
    printf("::CLEAR:\n");

    while (*list != NULL) {
        delete(list, *list);
        /* ������ ��� ������� �������� ������� */
    }
}

/* ��������� ������ ����� �� ����� */
COUNTRY* load()
{
    printf("::LOAD\n");
    char buf[MAX_LINE_LENGTH + 1];
    char* par[3];
    int cnt, pcount = 0;
    COUNTRY* p, * list = NULL;
    FILE* f = fopen(DBNAME, "r");

    buf[MAX_LINE_LENGTH] = 0x00;

    if (f) {
        while (fgets(buf, MAX_LINE_LENGTH, f)) {
            pcount = 0;
            par[pcount++] = buf;
            cnt = 0;
            while (buf[cnt]) {
                if (buf[cnt] == ',') {
                    buf[cnt] = 0x00;
                    par[pcount++] = &buf[cnt + 1];
                }
                cnt++;
            }
            if (pcount == 3) {
                add(&list, par[0], atoi(par[1]), atoi(par[2]));
            }
        }
        fclose(f);
    }
    return list;
}

/* ��������� ������ ����� � ���� */
void save(COUNTRY* list)
{
    FILE* f = fopen(DBNAME, "w+");
    COUNTRY* p = list;

    if (f) {
        while (p != NULL) {
            fprintf(f, "%s, %d, %d\n", p->name, p->population, p->area);
            p = p->next;
        }

        fclose(f);
    }
}


